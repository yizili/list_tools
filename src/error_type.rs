use crate::system_break;
use crate::tools::debug_tools::debug_result;
use std::fmt::{Debug, Display};

/// 错误类型
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
#[repr(Rust)]
pub enum ErrorType {
    IndexNotFound(usize),
    TestNotFound(String),
    NotFound(String),
}
impl Debug for ErrorType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IndexNotFound(n) => write!(f, "index not found: {}", n),
            Self::TestNotFound(string) => write!(f, "test not found: {}", string),
            Self::NotFound(string) => write!(f, "not found: {}", string),
        }
    }
}
impl Display for ErrorType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IndexNotFound(n) => write!(f, "index not found: {}", n),
            Self::TestNotFound(string) => write!(f, "test not found: {}", string),
            Self::NotFound(string) => write!(f, "not found: {}", string),
        }
    }
}
impl From<ErrorType> for (usize, String) {
    fn from(value: ErrorType) -> Self {
        match value {
            ErrorType::IndexNotFound(n) => (0, n.to_string()),
            ErrorType::TestNotFound(string) => (1, string),
            ErrorType::NotFound(string) => (1, string),
        }
    }
}
impl From<(usize, String)> for ErrorType {
    fn from(value: (usize, String)) -> Self {
        match value.0 {
            0 => ErrorType::IndexNotFound(debug_result(value.1.parse(), Some("can't to number"))),
            1 => ErrorType::TestNotFound(value.1),
            2 => ErrorType::NotFound(value.1),
            _ => system_break!("can't to ErrorType!"),
        }
    }
}
impl From<[String; 2]> for ErrorType {
    fn from(value: [String; 2]) -> Self {
        let index = match value[0].parse::<isize>() {
            Ok(t) => t,
            Err(_) => system_break!("can't to number: {}", value[0]),
        };
        match index {
            0 => (0, value[1].clone()).into(),
            1 => (1, value[1].clone()).into(),
            2 => (3, value[1].clone()).into(),
            _ => system_break!("can't to ErrorType!"),
        }
    }
}
impl From<ErrorType> for [String; 2] {
    fn from(value: ErrorType) -> Self {
        match value {
            ErrorType::IndexNotFound(i) => [0.to_string(), i.to_string()],
            ErrorType::TestNotFound(s) => [1.to_string(), s],
            ErrorType::NotFound(s) => [3.to_string(), s],
        }
    }
}
