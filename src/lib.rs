mod error_type;
mod fasts;
mod tools;
pub use error_type::*;
pub use fasts::*;
use std::{
    collections::HashSet,
    fmt::{Debug, Display},
    ops::{Add, Index, Neg, Sub},
};
use tools::Eprinter;

/* #region */
//TODO MARK:List struct
/// [`List`]是一个基于“struct递归”的结构体，他由于不基于`Rust`自带的[`Vec`]，所以作者得从零搭建：）。有不好的地方可以自己写（VSCode里Ctrl+左键。放心，协议是`GPL-3.0`
///
/// # Examples
///
/// ```rust
/// use list_tools::{clist, List};
/// let mut l: List<u8> = clist!(1, 1, 2, 4);
/// l.remove_of_index(0);
/// l.append_of_end(5);
/// l.append_of_front(0);
/// l.append_of_index(3, 3);
/// assert_eq!(Vec::from(l.clone()), vec![0, 1, 2, 3, 4, 5]);
/// assert_eq!(&l, &clist!(0, 1, 2, 3, 4, 5));
/// assert_ne!(&l, &clist!());
/// println!("{}", l);
/// println!("{:?}", l);
/// println!("{:#?}", l);
///
/// // --snip--
/// println!("---snip---");
///
/// let mut l = clist!(0; 10);
/// assert_eq!(l, clist!(0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
/// println!("{}", l);
/// println!("{:?}", l);
/// println!("{:#?}", l);
/// l.clear();
/// assert_eq!(l, List::new());
/// ```
#[derive(Clone)]
#[repr(Rust)]
pub struct List<T>(Option<(T, Box<List<T>>)>);

impl<T> List<T> {
    pub const NEW: List<T> = Self::new();

    /// 创建空[`List`]
    ///
    /// # Examples
    ///
    /// ```rust
    /// let l = list_tools::List::<()>::new();
    /// assert_eq!(l, list_tools::clist!());
    /// ```
    pub const fn new() -> List<T> {
        Self(None)
    }

    /// 从前插入
    ///
    /// # Examples
    ///
    /// ```rust
    /// let mut l = list_tools::clist!(1, 2, 3, 4);
    /// l.append_of_front(0);
    /// assert_eq!(l, list_tools::clist!(0, 1, 2, 3, 4));
    /// assert_ne!(l, list_tools::clist!(1, 2, 3, 4, 0));
    /// ```
    pub fn append_of_front(&mut self, front_t: T) -> &mut Self {
        let all = self.0.take();
        self.0 = Some((front_t, Box::new(List(all))));
        self
    }

    /// 从后插入
    ///
    /// # Examples
    ///
    /// ```rust
    /// let mut l = list_tools::clist!(1, 2, 3, 4);
    /// l.append_of_end(5);
    /// assert_eq!(l, list_tools::clist!(1, 2, 3, 4, 5));
    /// ```
    pub fn append_of_end(&mut self, end_t: T) -> &mut Self {
        match self.0 {
            Some((_, ref mut child)) => child.append_of_end(end_t),
            None => self.append_of_front(end_t),
        }
    }

    /// 根据`index`插入
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::{List, clist};
    /// let mut l = List::new();
    /// l.append_of_end(1);
    /// l.append_of_end(3);
    /// l.append_of_end(4);
    /// l.append_of_index(1, 2);
    /// l.append_of_index(0, 0);
    /// assert_eq!(l, clist!(0, 1, 2, 3, 4));
    /// ```
    pub fn append_of_index(&mut self, index: usize, add_t: T) -> &mut Self
    where
        T: Clone,
    {
        let mut x = Vec::<T>::new();
        for _ in index..self.len() {
            x.push(
                self.remove_of_index(index)
                    .eprinter(&format!("Can't append of index: {}", index)),
            );
        }
        self.append_of_end(add_t);
        for i in x {
            self.append_of_end(i);
        }
        self
    }

    #[deprecated(since = "0.1.8", note = "请转用`remove_of_index`，参数完全一样")]
    pub fn remove_index() {
        // !!!不要来看了!!!
    }

    /// 提取[`List`]的第`index`个(直接取出，不留在内部)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::{List, clist};
    /// let mut l: List<u8> = List::new();
    /// l.append_of_end(1);
    /// l.append_of_end(2);
    /// l.append_of_end(3);
    /// l.append_of_end(4);
    /// l.remove_of_index(2);
    /// assert_eq!(l, clist!(1, 2, 4));
    /// ```
    ///
    /// # Panic
    ///
    /// 可能在取出的时候发现没有那个数据，于是就会返回[`ErrorType`]
    pub fn remove_of_index(&mut self, index: usize) -> Result<T, ErrorType>
    where
        T: Clone,
    {
        let mut new_self = self.to_vec();
        match self.get_t_of_index(index) {
            Ok(_) => (),
            Err(e) => return Err(e),
        }
        let res = new_self.remove(index);
        *self = Self::vec_to_list(new_self);
        Ok(res)
    }

    /// 提取[`List`]的第一个数据(直接取出，不留在内部)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::{List, clist};
    /// let mut l: List<u8> = clist!(1, 2, 3, 4);
    /// assert_eq!(l.remove_of_front().unwrap(), 1);
    /// assert_eq!(l, clist!(2, 3, 4));
    /// ```
    /// # Panic
    ///
    /// 可能在取出的时候发现没有那个数据，于是就会返回[`ErrorType`]
    pub fn remove_of_front(&mut self) -> Result<T, ErrorType>
    where
        T: Clone,
    {
        self.remove_of_index(0)
    }
    /// 提取[`List`]的最后一个数据(直接取出，不留在内部)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::{List, clist};
    /// let mut l: List<u8> = clist!(1, 2, 3, 4);
    /// assert_eq!(l.remove_of_end().unwrap(), 4);
    /// assert_eq!(l, clist!(1, 2, 3));
    /// ```
    /// # Panic
    ///
    /// 可能在取出的时候发现没有那个数据，于是就会返回[`ErrorType`]
    pub fn remove_of_end(&mut self) -> Result<T, ErrorType>
    where
        T: Clone,
    {
        self.remove_of_index(self.len() - 1)
    }

    /// 提取[`List`]的第1个(保留在内部)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::List;
    /// fn result_to_bool<A, B>(result: Result<A, B>) -> bool {
    ///     match result {
    ///         Ok(_) => true,
    ///         Err(_) => false,
    ///     }
    /// }
    /// let mut l: List<u8> = List::new();
    /// l.append_of_end(1);
    /// l.append_of_end(2);
    /// l.append_of_end(3);
    /// l.append_of_end(4);
    /// assert!(result_to_bool(l.get_t_of_index(3)));
    /// ```
    pub const fn get_t_of_front(&self) -> Result<&T, ErrorType> {
        self.get_t_of_index(0)
    }

    /// 提取[`List`]的第`index`个(保留在内部)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::List;
    /// fn result_to_bool<A, B>(result: Result<A, B>) -> bool {
    ///     match result {
    ///         Ok(_) => true,
    ///         Err(_) => false,
    ///     }
    /// }
    /// let mut l: List<u8> = List::new();
    /// l.append_of_end(1);
    /// l.append_of_end(2);
    /// l.append_of_end(3);
    /// l.append_of_end(4);
    /// assert!(result_to_bool(l.get_t_of_index(3)));
    /// ```
    pub const fn get_t_of_index(&self, index: usize) -> Result<&T, ErrorType> {
        let mut a = &self.0;
        let mut i = 0;
        loop {
            if i == index {
                match a {
                    Some((t, _)) => return Ok(t),
                    None => return Err(ErrorType::IndexNotFound(i)),
                }
            } else {
                match a {
                    Some((_, b)) => a = &b.0,
                    None => return Err(ErrorType::IndexNotFound(i)),
                }
            }
            i += 1;
        }
    }

    /// 提取[`List`]的第1个(保留在内部)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::List;
    /// let mut l: List<u8> = List::new();
    /// l.append_of_end(1);
    /// l.append_of_end(2);
    /// l.append_of_end(3);
    /// l.append_of_end(4);
    /// assert_eq!(l.get_t_of_index(3), Ok(&4));
    /// ```
    pub const fn get_t_of_end(&self) -> Result<&T, ErrorType> {
        self.get_t_of_index(self.len() - 1)
    }

    /// 获取[`List`]的长度
    ///
    /// # EXamples
    ///
    /// ```
    /// use list_tools::List;
    /// let mut l: List<u8> = List::new();
    /// l.append_of_end(1);
    /// l.append_of_end(2);
    /// l.append_of_end(3);
    /// l.append_of_end(4);
    /// assert_eq!(4, l.len());
    /// l.append_of_end(5);
    /// assert_ne!(4, l.len());
    /// ```
    pub const fn len(&self) -> usize {
        let mut a = &self.0;
        let mut i = 0;
        loop {
            match a {
                Some((_, b)) => a = &b.0,
                None => return i,
            }
            i += 1;
        }
    }

    /// 把[`List`]值放进来，我们会生成一个新[`Vec`]
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::List;
    /// let l = List::vec_to_list(vec![1, 2, 3]);
    /// assert_eq!(vec![1, 2, 3], l.to_vec());
    /// ```
    pub fn to_vec(&self) -> Vec<T>
    where
        T: Clone,
    {
        let mut vec = Vec::<T>::new();
        let mut a = &self.0;
        loop {
            match a {
                Some((t, b)) => {
                    vec.push(t.clone());
                    a = &b.0;
                }
                None => break,
            }
        }
        vec
    }

    /// 把[`Vec`]值放进来，我们会生成一个新[`List`]
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::List;
    /// let l = List::vec_to_list(vec![0, 1, 2]);
    /// println!("{}", l);
    /// ```
    pub fn vec_to_list(vec: Vec<T>) -> List<T> {
        let mut res: List<T> = List::new();
        for t in vec {
            res.append_of_end(t);
        }
        res
    }

    /// 此方法将清空[`List`]
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::List;
    /// let mut l = List::<u8>::new();
    /// l.append_of_end(0);
    /// l.append_of_end(1);
    /// l.append_of_end(2);
    /// assert_eq!(l, List::vec_to_list(vec![0, 1, 2]));
    /// l.clear();
    /// assert_eq!(l, List::new());
    /// ```
    pub fn clear(&mut self) {
        *self = Self::new();
    }

    #[deprecated(since = "0.1.8", note = "请使用`slice_to_list`")]
    /// 把一个\[T\]给我们，我们会返回一个[`List`]
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::{List, clist};
    /// let l = List::from(&[1, 2, 3]);
    /// assert_eq!(l, clist!(1, 2, 3));
    /// ```
    pub fn from(t: &[T]) -> Self
    where
        T: Clone,
    {
        let mut res = Self::new();
        for arg in t {
            res.append_of_end(arg.clone());
        }
        res
    }

    /// 将自身颠倒(`&mut self`)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::{List, clist};
    /// let mut l = List::vec_to_list(vec![3, 2, 1]);
    /// l.reverse_mut();
    /// assert_eq!(clist!(1, 2, 3), l);
    /// ```
    pub fn reverse_mut(&mut self)
    where
        T: Clone,
    {
        let mut vec = self.clone().to_vec();
        vec.reverse();
        *self = vec.into();
    }

    /// 将自身颠倒(`&self`)
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::*;
    /// let l: List<i32> = clist!(1, 2, 3);
    /// assert_eq!(l.reverse(), clist!(3, 2, 1));
    /// ```
    pub fn reverse(&self) -> Self
    where
        T: Clone,
    {
        let mut l = self.to_vec();
        l.reverse();
        Self::vec_to_list(l)
    }

    /// 寻找某个东西
    ///
    /// # Example
    ///
    /// ```rust
    /// use list_tools::*;
    /// let l = clist!(1, 2, 3, 4, 5);
    /// assert_eq!(l.search(1), vec![0]);
    /// assert_eq!(l.search(0), vec![]);
    /// ```
    /// Some time, have two......
    /// ```rust
    /// use list_tools::*;
    /// let l = clist!(1, 2, 3, 4, 2);
    /// assert_eq!(l.search(2), vec![1, 4]);
    /// ```
    /// return first one
    pub fn search(&self, find_t: T) -> Vec<usize>
    where
        T: Clone + Ord,
    {
        let mut vec = Vec::new();
        for (i, t) in self.to_vec().iter().enumerate() {
            if t == &find_t {
                vec.push(i)
            }
        }
        vec
    }

    /// 搜索（返回第一个搜索到的`index`）
    ///
    /// # Example
    ///
    /// ```rust
    /// use list_tools::*;
    /// let l = clist!(1, 2, 3, 2, 1);
    /// assert_eq!(l.search_of_one(1), Some(0));
    /// assert_eq!(l.search_of_one(4), None);
    /// ```
    pub fn search_of_one(&self, find_t: T) -> Option<usize>
    where
        T: Clone + Ord,
    {
        match self.search(find_t).get(0) {
            Some(t) => Some(*t),
            None => None,
        }
    }

    /// 排序
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::*;
    /// let l = clist!(4, 2, 3, 1);
    /// assert_eq!(l.sorted(), clist!(1, 2, 3, 4));
    /// ```
    pub fn sorted(&self) -> Self
    where
        T: Clone + Ord,
    {
        let mut s = self.to_vec();
        s.sort();
        Self::vec_to_list(s)
    }

    /// 排序
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::clist;
    /// let mut l = clist!(4, 2, 3, 1);
    /// l.mut_sorted();
    /// assert_eq!(l, clist!(1, 2, 3, 4));
    /// ```
    pub fn mut_sorted(&mut self)
    where
        T: Ord + Clone,
    {
        *self = self.sorted();
    }

    /// `[T]` -> [`List`]
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::*;
    /// let l = List::slice_to_list(&[0, 1, 2, 3]);
    /// assert_eq!(l, clist!(0, 1, 2, 3));
    /// ```
    pub fn slice_to_list(s: &[T]) -> Self
    where
        T: Clone,
    {
        Self::vec_to_list(s.to_vec())
    }

    /// 转换两个值(根据`index`)
    ///
    /// # Panic
    ///
    /// 当输入的数值无法转换成`usize`，就无法实现
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::*;
    /// let mut l = clist!(0, 3, 2, 1);
    /// l.swap_of_index(1u8, 3u8);
    /// assert_eq!(l, clist!(0, 1, 2, 3))
    /// ```
    pub fn swap_of_index<N>(&mut self, f_index: N, s_index: N)
    where
        T: Clone,
        usize: From<N>,
    {
        let mut clone_self: Vec<T> = self.to_vec();
        clone_self.swap(f_index.into(), s_index.into());
        *self = Self::vec_to_list(clone_self);
    }

    /// 转换两个值(根据`T`)
    ///
    /// 请不要尝试输入两个同样的值，也不要在[`List`]里输入两个一样的值，否则只会发现第一个。。。
    ///
    /// # Panic
    ///
    /// 当输入的值不在"self"内，就无法实现
    ///
    /// # Examples
    ///
    /// ```rust
    /// use list_tools::*;
    /// let mut l = clist!(0, 3, 2, 1);
    /// l.swap_of_t(3, 1).unwrap();
    /// assert_eq!(l, clist!(0, 1, 2, 3))
    /// ```
    pub fn swap_of_t(&mut self, f: T, s: T) -> Option<()>
    where
        T: Ord + Clone + ToString,
    {
        let f_index = self.search_of_one(f.clone())?;
        let s_index = self.search_of_one(s.clone())?;
        self.swap_of_index(f_index, s_index);
        Some(())
    }
}

impl<T: Ord + Clone> Ord for List<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let (self_vec, orther_vec) = (self.to_vec(), other.to_vec());
        self_vec.cmp(&orther_vec)
    }
}

impl<T: PartialOrd> PartialOrd for List<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T: Eq> Eq for List<T> {}

impl<T: PartialEq> PartialEq for List<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

/// [`Display`] display function for [`List`]
impl<T: Clone + Debug> Display for List<T> {
    /// [`Display`] display function for [`List`]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.to_vec())
    }
}

/// [`Debug`] display function for [`List`]
impl<T: Clone + Debug> Debug for List<T> {
    /// [`Debug`] display function for [`List`]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.to_vec())
    }
}

/// [`Default`] function for [`List`]
impl<T> Default for List<T> {
    /// [`Default`] function for [`List`]
    fn default() -> Self {
        Self::new()
    }
}

/// [`List<T>`] 转 [`Vec<T>`]
impl<T: Clone> From<List<T>> for Vec<T> {
    /// [`List<T>`] 转 [`Vec<T>`]
    fn from(value: List<T>) -> Self {
        value.to_vec()
    }
}

/// [`Vec<T>`] 转 [`List<T>`]
impl<T> From<Vec<T>> for List<T> {
    /// [`Vec<T>`] 转 [`List<T>`]
    fn from(value: Vec<T>) -> Self {
        Self::vec_to_list(value)
    }
}

/// /[T/]
impl<T: Clone> From<&[T]> for List<T> {
    fn from(value: &[T]) -> Self {
        List::slice_to_list(value)
    }
}

impl<T: Clone> From<Box<[T]>> for List<T> {
    fn from(value: Box<[T]>) -> Self {
        List::slice_to_list(&value)
    }
}

impl<T: Clone> From<List<T>> for Box<[T]> {
    fn from(value: List<T>) -> Self {
        value.to_vec().into_boxed_slice()
    }
}

/// 让[`List`]支持遍历
///
/// # Examples
///
/// ```rust
/// use list_tools::{List, clist};
/// let mut l = clist!(0, 1, 2);
/// assert_eq!(l.next(), Some(0));
/// assert_eq!(l.next(), Some(1));
/// assert_eq!(l.next(), Some(2));
/// assert_eq!(l.next(), None);
/// ```
impl<T: Clone> Iterator for List<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match &self.0 {
            Some(_) => {
                let mut vec = self.to_vec();
                let res = vec.remove(0);
                *self = Self::vec_to_list(vec);
                Some(res)
            }
            None => None,
        }
    }
}

/// 让`&List`支持遍历
///
/// # Examples
///
/// ```rust
/// use list_tools::{List, clist};
/// let mut l = clist!(0, 1, 2);
/// assert_eq!(l.next(), Some(0));
/// assert_eq!(l.next(), Some(1));
/// assert_eq!(l.next(), Some(2));
/// assert_eq!(l.next(), None);
/// ```
impl<T: Clone> Iterator for &List<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.clone().next()
    }
}

/// 为[`List`]实现加法
///
/// # Examples
///
/// ```rust
/// use list_tools::*;
/// let l1 = clist!(1, 2);
/// let l2 = clist!(3, 4);
/// assert_eq!(l1.clone()+l2.clone(), clist!(1 ,2, 3, 4));
/// assert_eq!(l2+l1, clist!(3, 4, 1, 2));
/// ```
impl<T: Clone> Add for List<T> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        let mut copy_self = self;
        for t in rhs.to_vec() {
            copy_self.append_of_end(t);
        }
        copy_self
    }
}

/// 为`&List`实现加法
///
/// # Examples
///
/// ```rust
/// use list_tools::*;
/// let l1 = clist!(1, 2);
/// let l2 = clist!(3, 4);
/// assert_eq!(&l1+&l2, clist!(1 ,2, 3, 4));
/// assert_eq!(&l2+&l1, clist!(3 ,4 ,1 ,2));
/// ```
impl<T: Clone> Add for &List<T> {
    type Output = List<T>;

    fn add(self, rhs: Self) -> List<T> {
        let mut copy_self = self.clone();
        for t in rhs.to_vec() {
            copy_self.append_of_end(t);
        }
        copy_self
    }
}

/// 为[`List`]实现减法
///
/// # Examples
///
/// ```rust
/// use list_tools::*;
/// let l1 = clist!(1, 2, 3);
/// let l2 = clist!(2, 3, 4);
/// assert_eq!(l1-l2, clist!(1, 4));
/// ```
impl<T: Ord + Clone + ToString> Sub for List<T> {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        let mut res = List::new();
        for index in rhs.clone() {
            if let None = self.clone().search_of_one(index.clone()) {
                res.append_of_end(index);
            }
        }
        for index in self.clone() {
            if let None = rhs.clone().search_of_one(index.clone()) {
                if let None = res.search_of_one(index.clone()) {
                    res.append_of_end(index);
                }
            }
        }
        res.sorted()
    }
}

/// 为`&List`实现减法
///
/// # Examples
///
/// ```rust
/// use list_tools::*;
/// let l1 = clist!(1, 2, 3);
/// let l2 = clist!(2, 3, 4);
/// assert_eq!(&l1-&l2, clist!(1, 4));
/// ```
impl<T: Ord + Clone + ToString> Sub for &List<T> {
    type Output = List<T>;
    fn sub(self, rhs: Self) -> Self::Output {
        self.clone() - rhs.clone()
    }
}

/// 为[`List`]实现翻转
///
/// # Examples
///
/// ```rust
/// use list_tools::*;
/// let l = clist!(1,2,3);
/// assert_eq!(-l, clist!(3,2,1));
/// ```
impl<T: Clone> Neg for List<T> {
    type Output = Self;

    fn neg(self) -> Self {
        self.reverse()
    }
}

/// 为`&List`实现翻转
///
/// # Examples
///
/// ```rust
/// use list_tools::*;
/// let l = clist!(1,2,3);
/// assert_eq!(-&l, clist!(3,2,1));
/// ```
impl<T: Clone> Neg for &List<T> {
    type Output = List<T>;

    fn neg(self) -> Self::Output {
        self.reverse()
    }
}

impl<T> Index<usize> for List<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        self.get_t_of_index(index)
            .eprinter(&format!("Index out of len!(len: {})", self.len()))
    }
}

impl<C> FromIterator<C> for List<C> {
    fn from_iter<T: IntoIterator<Item = C>>(iter: T) -> Self {
        let mut list = List::new();
        for i in iter {
            list.append_of_end(i);
        }
        list
    }
}
/* #endregion */
pub trait ToList<T>
where
    T: Clone,
{
    fn to_list(self) -> List<T>;
}

impl<T: Clone> ToList<T> for Vec<T> {
    fn to_list(self) -> List<T> {
        List::vec_to_list(self)
    }
}

impl<T: Clone> ToList<T> for &[T] {
    fn to_list(self) -> List<T> {
        self.into()
    }
}

impl<T: Clone> ToList<T> for HashSet<T> {
    fn to_list(self) -> List<T> {
        self.into_iter().collect()
    }
}
