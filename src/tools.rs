use crate::tools::debug_tools::exit;

#[repr(Rust)]
pub mod debug_tools {
    use crate::system_break;
    use std::fmt::Debug;
    #[repr(Rust)]
    #[allow(dead_code)]
    pub fn debug_bool(r#is: bool, line: Option<usize>) {
        match r#is {
            true => true,
            false => match line {
                Some(n) => exit(format!("Is not true, break in line {}", n)),
                None => system_break!("Is not true, break"),
            },
        };
    }

    #[repr(Rust)]
    pub fn debug_result<T, E: Debug, O: Debug>(r#is: Result<T, E>, line: Option<O>) -> T {
        match r#is {
            Ok(t) => t,
            Err(e) => match line {
                Some(n) => system_break!("{:?} {:?}", e, n),
                None => system_break!("Is not ok block, break"),
            },
        }
    }

    #[repr(Rust)]
    #[allow(dead_code)]
    pub fn debug_option<T, O: Debug>(r#is: Option<T>, line: Option<O>) -> T {
        match r#is {
            Some(t) => t,
            None => match line {
                Some(n) => system_break!("Can't get option,( break test {:?} )", n),
                None => system_break!("Can't get option, break"),
            },
        }
    }

    #[repr(Rust)]
    pub fn exit<T: Debug>(t: T) -> ! {
        println!("Exit Test: {:?}", t);
        std::process::exit(0);
    }
}

#[macro_export]
#[repr(Rust)]
macro_rules! system_break {
    ($($t:expr),*) => {
        crate::tools::debug_tools::exit(format!($($t),*))
    };
    ($t:expr, $($args:tt),*) => {
        crate::tools::debug_tools::exit(format!($t, $($args),*))
    };
    () => {
        crate::tools::debug_tools::exit("None")
    };
}

pub trait Eprinter {
    type T;
    type E;
    fn eprinter(self, err: &str) -> Self::T;
    fn uneprinter(self, err: &str) -> Self::E;
}

impl<T, E> Eprinter for Result<T, E> {
    type T = T;
    type E = E;
    fn eprinter(self, err: &str) -> T {
        match self {
            Ok(t) => t,
            Err(_) => exit(err),
        }
    }
    fn uneprinter(self, err: &str) -> E {
        match self {
            Ok(_) => exit(err),
            Err(e) => e,
        }
    }
}

impl<T> Eprinter for Option<T> {
    type T = T;
    type E = ();
    fn eprinter(self, err: &str) -> T {
        match self {
            Some(t) => t,
            None => exit(err),
        }
    }
    fn uneprinter(self, err: &str) -> () {
        match self {
            Some(_) => exit(err),
            None => (),
        }
    }
}
