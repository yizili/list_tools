/// 快速生成[`crate::List`]
///
/// # Examples
///
/// ```rust
/// use list_tools::{List, clist};
/// assert_eq!(clist!(1, 2), List::vec_to_list(vec![1, 2]));
/// ```
#[macro_export]
#[repr(Rust)]
macro_rules! clist {
    () => {
        list_tools::List::new()
    };
    ($arg:expr; $n:expr) => {
        list_tools::List::vec_to_list(vec![$arg; $n])
    };
    ($($x:expr),+) => {
        {
            let mut l = list_tools::List::new();
            $(
                l.append_of_end($x);
            )+
            l
        }
    };
}

/// [`crate::List`]的快速操作
///
/// # Examples
///
/// ```rust
/// use list_tools::{fast_tools, clist};
/// assert_eq!(fast_tools::vec_to_list(vec![0, 1, 2]), clist!(0, 1, 2));
/// assert_eq!(fast_tools::slice_to_list(&[0, 1, 2]), clist!(0, 1, 2));
/// assert_eq!(fast_tools::string_to_list("0 1 2".to_string(), " ".to_string()), clist!(0.to_string(), 1.to_string(), 2.to_string()));
/// ```
#[repr(Rust)]
pub mod fast_tools {
    use crate::List;

    /// This function can do [`Vec`] to [`crate::List`]
    #[repr(Rust)]
    pub fn vec_to_list<T>(v: Vec<T>) -> List<T> {
        List::vec_to_list(v)
    }
    /// This function can do `&[T]` to [`crate::List`]
    #[repr(Rust)]
    pub fn slice_to_list<T: Clone>(s: &[T]) -> List<T> {
        List::slice_to_list(s)
    }
    /// This function can do [`String`] to [`crate::List`] (args are [`String`])
    #[repr(Rust)]
    pub fn string_to_list<S: ToString>(t: String, split_test: S) -> List<String> {
        let split_test = split_test.to_string();
        let split = t.split(&split_test);
        let collect = split.collect::<Vec<&str>>();
        let mut l = List::new();
        for i in collect {
            l.append_of_end(i.to_string());
        }
        l
    }
    /// This function can do &[`str`] to [`crate::List`] (args are [`String`])
    #[repr(Rust)]
    pub fn str_to_list<S: ToString>(t: &str, split_test: S) -> List<String> {
        let split_test = split_test.to_string();
        let split = t.split(&split_test);
        let collect = split.collect::<Vec<&str>>();
        let mut l = List::new();
        for i in collect {
            l.append_of_end(i.to_string());
        }
        l
    }

    /// This function can do [`String`] to [`crate::List`] (args are [`char`])
    #[repr(Rust)]
    pub fn string_to_list_char<S: ToString>(t: String, split_test: char) -> List<String> {
        let split = t.split(split_test);
        let collect = split.collect::<Vec<&str>>();
        let mut l = List::new();
        let _: Vec<()> = vec![];
        for i in collect {
            l.append_of_end(i.to_string());
        }
        l
    }
}
