# list_tools

## 介绍

[`List`]是一个基于“struct递归”的结构体，他由于不基于`Rust`自带的[`Vec`]，所以作者得从零搭建：）。
有不好的地方可以自己写（VSCode里Ctrl+左键）。放心，协议是`GPL-3.0`
已发布：

+ 0.1.0: 添加List
+ 0.1.1: 给List添加reverse
+ 0.1.2: 修改0.1.1
+ 0.1.3: 给List添加Iter  
+ 0.1.4: 给List添加Add trait+Sub trait+find+sorted+mut_sorted
+ 0.1.5: 实现ErrorType<-->(usize, String)，给List实现reserve+reserve_mut+slice_to_list+Neg，增加模块组fast_tools
+ 0.1.6: 实现ErrorType<-->[String; 2]
+ 0.1.7: 给List实现swap_of_index, swap_of_t
+ 0.1.8: 给List实现remove_of_index(取代remove_index)，remove_of_front, remove_of_end, get_of_front
+ 0.1.9: 给List实现Index\<T> FromIterator\<T>，添加方法search_of_one，修改方法search，添加ToList特征

crate地址：<https://crates.io/crates/list_tools>  
gitee地址：<https://gitee.com/yizili/list_tools/edit/master>

## Examples

```rust
use list_tools::*;
let mut l: List<u8> = clist!(1, 1, 2, 4);
let _ = l.remove_of_index(0);
l.append_of_end(5);
l.append_of_front(0);
l.append_of_index(3, 3);
if Vec::from(l.clone()) == vec![0, 1, 2, 3, 4, 5]
    && Vec::from(l.clone()).to_list() == clist!(0, 1, 2, 3, 4, 5)
{
    println!("oh yea");
}
assert_eq!(l.get_t_of_index(0).unwrap(), &0);
assert_eq!(l.get_t_of_index(1).unwrap(), &1);
assert_eq!(l.get_t_of_index(2).unwrap(), &2);
assert_eq!(&l, &clist!(0, 1, 2, 3, 4, 5));
assert_ne!(&l, &clist!());
println!("{}", l);
println!("{:?}", l);
println!("{:#?}", l);

// --snip--
println!("---snip---");

let mut l = clist!(0; 10);
assert_eq!(l, clist!(0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
println!("{}", l);
println!("{:?}", l);
println!("{:#?}", l);
assert_eq!(l.reverse(), clist!(0; 10));
l.clear();
assert_eq!(l, List::new());

// --snip--
println!("---snip---");

let l = clist!(1, 2, 3, 4, 5);
assert_eq!(l[0], *l.get_t_of_index(0).unwrap());
```

## 其他

反馈发邮箱：<3396311242@qq.com>
